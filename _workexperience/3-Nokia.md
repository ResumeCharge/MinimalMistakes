---
layout: single
title: Nokia
permalink: /workexperience/Nokia
---
### Date : Dec. 2017 - Apr. 2019
### Location : Toronto, ON
### Title : Telecommunications Lead

#### Technologies
* **Angular**
* **Java**
* **Google Web Toolkit**
* **Hibernate**
* **SQL**
* **Oracle DMBS**