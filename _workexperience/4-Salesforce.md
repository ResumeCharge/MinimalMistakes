---
layout: single
title: Salesforce
permalink: /workexperience/Salesforce
---
### Date : Jan. 2020 - Present
### Location : Virtual
### Title : Software Development Manager

#### Skills
* **Management**
* **Project Planning**
* **Scheduling**
* **Resource Management**

