---
layout: single
title: Mitel
permalink: /workexperience/Mitel
---
### Date : March 2013 - Dec. 2017
### Location : Ottawa, ON
### Title : Software Apprentice

#### Technologies
* **React**
* **Angular**
* **VoIP**
* **WebRTC**
* **Typescript**
