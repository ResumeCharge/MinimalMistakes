---
layout: single
permalink: /about/
author_profile: true
title: About me
subtitle: Developing software with pride
---

My name is Frank Dawson. I am a software development manager working at salesforce..

### My Hobbies
    - Skiing
    - Sailing
    - Woodworking
    - Clock Making
    - Video games
    - Blogging
